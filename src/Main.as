package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageAspectRatio;
	import flash.display.StageOrientation;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	
	import ui.BG;
	import ui.Discount;
	import ui.Gabe;
	
	[SWF(frameRate="60")]
	
	public class Main extends Sprite
	{
		
		private var _bg:BG;
		private var _discountArray:Array = [];
		private var _music:Sound;
		private var _musicChannel:SoundChannel;
		
		public function Main()
		{
			// Create the BG
			_bg = new BG();
			_bg.x = stage.stageWidth / 2;
			_bg.y = stage.stageHeight;
			this.addChild(_bg);
			
			// Create some inital discounts
			for (var i:int = 0; i < 10; i++) 
			{
				createDiscount();
			}
			
			// Create The Gabe
			createGabe();
			
			// Create the music
			createMusic();
			
			// Trigger the animation
			this.addEventListener(Event.ENTER_FRAME, onUpdate);
		}
		
		private function createDiscount():void
		{
			var discount:Discount = new Discount;
			discount.x = Math.random() * stage.stageWidth;
			discount.y = -50;
			discount.addEventListener(Discount.REMOVE_ME, onRemoveMe);
			
			_discountArray.push(discount);
			this.addChild(discount);
		}
		
		private function createGabe():void
		{
			var gabe:Gabe = new Gabe();
			gabe.x = stage.stageWidth / 2;
			gabe.y = stage.stageHeight + gabe.height;
			this.addChild(gabe);
		}
		
		private function createMusic():void
		{
			_music = new Sound(new URLRequest("music/music.mp3")); 
			_music.addEventListener(Event.COMPLETE, playMusic);
		}
		
		private function playMusic(event:Event):void
		{
			_musicChannel = _music.play(0, 9999);
			this.addEventListener(Event.DEACTIVATE, stopMusic);
			this.removeEventListener(Event.ACTIVATE, playMusic);
		}
		
		private function stopMusic(event:Event):void
		{
			_musicChannel.stop();
			this.removeEventListener(Event.DEACTIVATE, stopMusic);
			this.addEventListener(Event.ACTIVATE, playMusic);
		}
		
		private function onRemoveMe(event:Event):void
		{
			var discount:Discount = Discount(event.currentTarget);
			
			_discountArray.splice(_discountArray.indexOf(discount), 1);
			this.removeChild(discount);
			
			createDiscount();
		}
		
		private function onUpdate(event:Event):void
		{
			// Rotate the BG
			_bg.update();
			
			// Move all the discounts
			for each (var discount:Discount in _discountArray) 
			{
				discount.update();	
			}
		}
		
	}
}
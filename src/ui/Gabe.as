package ui
{
	import flash.events.Event;

	public class Gabe extends GabeBase
	{
		public function Gabe()
		{
			this.scaleX = this.scaleY = 0.5;
			this.addEventListener(Event.ADDED_TO_STAGE, stageAdded);
		}
		
		protected function stageAdded(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, stageAdded);
			this.addEventListener(Event.ENTER_FRAME, rise);
		}		
		
		public function rise(event:Event):void
		{
			this.y -= 0.5;
			
			if(this.y < stage.stageHeight){
				this.removeEventListener(Event.ENTER_FRAME, rise);
			}
		}
	}
}
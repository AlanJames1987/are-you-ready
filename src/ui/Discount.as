package ui
{
	import flash.events.Event;

	public class Discount extends DiscountBase
	{
		public static const REMOVE_ME:String = "remove_me";
		
		private var _scale:Number;
		
		public function Discount()
		{
			super();
			
			_scale = Math.random() * .75 + .25;
			this.scaleX = this.scaleY = _scale;
			
			var amount:uint = Math.random() * 50 + 40;
			amount = (Math.round(amount / 5)) * 5;
			this.tf_discount.text = '-' + amount + '%';
		}
		
		public function update():void
		{
			this.y += 10 * _scale;
			
			if(this.y > stage.stageHeight + this.height){
				this.dispatchEvent(new Event(Discount.REMOVE_ME));
			}
		}
	}
}